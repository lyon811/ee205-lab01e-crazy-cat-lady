###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d- Crazy Cat Lady - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.1 - Initial version
###
### Build a Crazy Cat Lady C program
###
### @author  Lyon <lyonws@hawaii.edu>
### @date     01_13_2021
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = crazyCatLady

all: $(TARGET)

crazyCatLady: crazyCatLady.cpp
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatLady.cpp

clean:
	rm -f $(TARGET) *.o
	










